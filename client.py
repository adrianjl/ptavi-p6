#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Programa cliente que abre un socket a un servidor."""

import socket
import sys

# Cliente UDP simple.

# Dirección IP del servidor.
# SERVER = 'localhost'.
# PORT = 6001.

try:
    metodo = sys.argv[1]
    sip_receptor = sys.argv[2].split('@')[0]
    print(sip_receptor)
    sip_ip = sys.argv[2].split('@')[1].split(':')[0]
    sip_port = int(sys.argv[2].split('@')[1].split(':')[1])
except (IndexError, ValueError):
    sys.exit("Usage: python3 client.py metodo receiver@IP:puerto")
# Contenido que vamos a enviar


mess = "metodo sip:receptor@ipx SIP/2.0\r\n"
sdp = "Content-Type: application/sdp\r\n\r\n"
sdp += "v=0\r\no=robin@gotham.com 127.0.0.1\r\ns=misesion\r\nt=0\r\n"
sdp += "m=audio 34543 RTP\r\n"


# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((sip_ip, sip_port))

    LINE = mess.replace("metodo", metodo).replace("receptor", sip_receptor)
    LINE = LINE.replace("ipx", sip_ip)
    if metodo == "INVITE":
        LINE += sdp
    print(LINE)
    my_socket.send(bytes(LINE, 'utf-8'))
    data = my_socket.recv(1024).decode("utf-8")
    print(data)
    if "100" in data and "180" in data:
        LINE = mess.replace("metodo", "ACK").replace("receptor", sip_receptor)
        LINE = LINE.replace("ipx", sip_ip)
        my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
    print("Terminando socket...")

print("Fin.")
