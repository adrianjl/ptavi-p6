#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socketserver
import sys
import simplertp
import secrets


class EchoHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    ip_rtp = None
    port_rtp = None

    def handle(self):
        """Funcion Handle."""
        data = self.rfile.read().decode("utf-8")
        print(data)
        metodo = data.split()[0]
        if metodo in ["INVITE", "ACK", "BYE"]:
            if data.split()[2] != "SIP/2.0":
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n")
            else:
                if metodo == "INVITE":
                    self.ip_rtp = data.split("\r\n")[4].split()[-1]
                    self.port_rtp = int(data.split("\r\n")[7].split()[1])
                    mess = "SIP/2.0 100 Trying\r\n\r\n"
                    mess += "SIP/2.0 180 Ringing\r\n\r\n SIP/2.0 200 OK\r\n"
                    self.wfile.write(bytes(mess, "utf-8"))
                if metodo == "ACK":
                    if self.ip_rtp is not None and self.port_rtp is not None:
                        BIT = secrets.randbelow(1)
                        RTP_header = simplertp.RtpHeader()
                        RTP_header.set_header(version=2, marker=BIT,
                                              payload_type=14, ssrc=200002)
                        audio = simplertp.RtpPayloadMp3(audios)
                        simplertp.send_rtp_packet(RTP_header, audio,
                                                  self.ip_rtp, self.port_rtp)
                        self.ip_rtp = None
                        self.port_rtp = None
                if metodo == "BYE":
                    self.wfile.write(b"SIP/2.0 200 OK\r\n")
        else:
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n")


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    try:
        ip = sys.argv[1]
        puerto = int(sys.argv[2])
        audios = sys.argv[3]
    except(IndexError, ValueError):
        sys.exit("Usage: python3 server.py ip puerto audio_file")

    serv = socketserver.UDPServer((ip, puerto), EchoHandler)
    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        sys.exit("Finalizando servidor")
